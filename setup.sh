#! /usr/bin/env bash

echo " # -- Welcome to the opendroid setup -- # "
echo " "
echo " # -- Do you want to start setup? -- #"
echo "1) Yes"
echo "2) No"
read startnow

# Algerith to start or cancel setup
if [ "${startnow}" = "1" ]; then clear; fi
if [ "${startnow}" = "2" ]; then exit; fi